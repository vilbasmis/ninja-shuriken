﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shuriken : MonoBehaviour
{
    [Header("Spinning")]
    [SerializeField] float spinningSpeed;
    [SerializeField] int spinningDirection = 1;

    [Header("Movement")]
    [SerializeField] float shurikenSpeed;

    Vector2 movementDirection;
    private Rigidbody2D rigidBody;

	// Use this for initialization
	void Start ()
    {
        rigidBody = GetComponent<Rigidbody2D>();

        rigidBody.velocity = movementDirection * shurikenSpeed * Time.deltaTime;
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Rotate shuriken
        transform.Rotate(0, 0, spinningSpeed * Time.deltaTime * spinningDirection);
    }

    //private void FixedUpdate()
    //{
    //    rigidBody.velocity = movementDirection * shurikenSpeed * Time.deltaTime;
    //}

    public void SpinningDirection(int direction)
    {
        //artı veya eski değer ile dönüş yöünü değiştir
        spinningDirection *= direction;
    }

    //Set movement direction
    public void MovementDirection(Vector2 direction)
    {
        movementDirection = direction;

        if(direction.x > 0)
        {
            SpinningDirection(1);
        }
        else
        {
            SpinningDirection(-1);
        }
    }
}
