﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    [SerializeField] float cameraPadding = 0.5f;
    [Header("UI Properties")]
    [SerializeField] Text scoreText;
    [SerializeField] Text gameOverText;
    [SerializeField] GameObject gameScorePanel;
    [SerializeField] GameObject gameOverPanel;

    [Header("Balloon Properties")]
    [SerializeField] float minTime;
    [SerializeField] float maxTime;
    [SerializeField] float timeCounter = 1f;
    [SerializeField] Transform balloonPosition;
    [SerializeField] GameObject balloon;

    [Header("Shuriken Properties")]
    [SerializeField] GameObject shuriken;
    [SerializeField] Transform shurikenStartPosition;
    [SerializeField] Text shurikenText;
    [SerializeField] int shurikenCount = 5;
    [SerializeField] GameObject[] shurikenImages;


    //Parameters
    public static GameManager instance;
    private float score = 0;
    private float minXPosition,
                  maxXPosition,
                  minYPosition,
                  maxYPosition,
                  instaPosition;
    private bool touchStart = false,
                 gameOver = false;
    private Vector2 pointA,
                    pointB,
                    offset,
                    direction;
    private GameObject newShuriken,
                       newBalloon;
    private int shurikenImageCount;


    //Awake function
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        SetUpCameraBoundries();
    }
  


    // Use this for initialization
    void Start ()
    {
        SetText();
        shurikenImageCount = shurikenImages.Length;

        scoreText.text = score.ToString();
	}
	
	// Update is called once per frame
	void Update ()
    {
        GetInput();
        Timer();
    }


    //Ekran sınırlarını belirle
    private void SetUpCameraBoundries()
    {
        minXPosition = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + cameraPadding;
        maxXPosition = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - cameraPadding;

        minYPosition = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + cameraPadding;
        maxYPosition = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - cameraPadding;
    }

    public void SetText()
    {
        shurikenText.text = shurikenCount + " Shuriken"; 
    }

    public void ShurikenCount()
    {
        shurikenCount -= 1;
        SetText();

        if(shurikenCount < shurikenImageCount)
        {
            shurikenImages[shurikenCount].SetActive(false);
        }

        if(shurikenCount <= 0)
        {
            ActivateGameOverPanel();
        }
    }

    private void ActivateGameOverPanel()
    {
        gameOver = true;
        StartCoroutine("GameOverPanel");
    }

    IEnumerator GameOverPanel()
    {
        yield return new WaitForSeconds(0.8f);
        gameScorePanel.SetActive(false);
        gameOverPanel.SetActive(true);

        gameOverText.text = score.ToString();
    }

    public void ScoreIncrement(float score)
    {
        this.score += score;

        scoreText.text = this.score.ToString();
    }

    #region Shuriken Functions
    private void GetInput()
    {
        if(shurikenCount > 0)
        {
            if (Input.touchCount > 0)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    pointA = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z));
                }

                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    touchStart = true;
                    pointB = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z));

                    TouchControl();
                }
            }
        }
    }

    private void TouchControl()
    {
        if (touchStart)
        {
            offset = pointB - pointA;
            direction = Vector2.ClampMagnitude(offset, 1.0f);
            
            //ekranın altına doğru gitmemeli
            if(direction.y > 0)
            {
                InstantiateShuriken();
            }
            touchStart = false;
        }
    }

    private void InstantiateShuriken()
    {
        newShuriken = Instantiate(shuriken, shurikenStartPosition.transform.position, Quaternion.identity);

        newShuriken.GetComponent<Shuriken>().MovementDirection(direction);

        ShurikenCount();
    }
    #endregion


    #region Balloon Functions
    private void Timer()
    {
        if(gameOver == false)
        {
            timeCounter -= Time.deltaTime;
            if (timeCounter <= 0f)
            {
                InstantiateBalloons();

                timeCounter = UnityEngine.Random.Range(minTime, maxTime);
            }
        }
    }

    private void InstantiateBalloons()
    {
        instaPosition = UnityEngine.Random.Range(minXPosition, maxXPosition);
        Vector3 position = new Vector3(instaPosition, balloonPosition.transform.position.y, 0);

        newBalloon = Instantiate(balloon, position, Quaternion.identity);
    } 
    #endregion
}
