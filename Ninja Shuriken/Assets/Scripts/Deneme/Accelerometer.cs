﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelerometer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //float temp = Input.acceleration.x;
        //Debug.Log(temp);

        transform.Translate(Input.acceleration.x, Input.acceleration.y, Input.acceleration.z);
	}
}
