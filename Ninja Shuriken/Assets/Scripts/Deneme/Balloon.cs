﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balloon : MonoBehaviour
{

    [SerializeField] float speed = 10f;
    [SerializeField] float minSpeed = 1f;
    [SerializeField] float maxSpeed = 10f;
    [SerializeField] GameObject effect;
    [SerializeField] float score = 10f;

    // Use this for initialization
    void Start ()
    {
        speed = Random.Range(minSpeed, maxSpeed);
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(0, speed * Time.deltaTime, 0);
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Shuriken")
        {
            GameManager.instance.ScoreIncrement(score);

            var newEffect = Instantiate(effect, transform.position, Quaternion.identity);
            Destroy(newEffect, 2f);
            Destroy(gameObject);
        }
    }
}
