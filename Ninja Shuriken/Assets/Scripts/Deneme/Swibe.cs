﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swibe : MonoBehaviour {

    [SerializeField] float maxTime,
                           minSwipeDistance,
                           moveSpeed;

    private float startTime,
                  endTime,
                  swipeTime,
                  swipeDistance;

    private Vector3 startPosition,
                    endPosition;
    private Touch touch;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if(touch.phase == TouchPhase.Began)
            {
                startTime = Time.time;
                startPosition = touch.position;
            }
            else if(touch.phase == TouchPhase.Ended)
            {
                endTime = Time.time;
                endPosition = touch.position;

                swipeDistance = (endPosition - startPosition).magnitude;
                swipeTime = endTime - startTime;

                //Debug.Log(swipeDistance);
                Debug.Log(endPosition - startPosition);

                if(swipeTime <= maxTime && swipeDistance >= minSwipeDistance)
                {
                    //Swipe();
                }
                
            }
        }
		
	}

    private void Swipe()
    {
        Vector2 distance = endPosition - startPosition;
        if(distance.x != 0)
        {
            Move();
        }

        if(distance.y > 0)
        {
            Jump();
        }
    }

    private void Jump()
    {
        Debug.Log("jump");
        //GetComponent<Rigidbody2D>().velocity = new Vector2 (GetComponent<Rigidbody2D>().velocity.x, moveSpeed * Time.deltaTime);
    }

    private void Move()
    {
        Debug.Log("move");
        //GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed * Time.deltaTime, GetComponent<Rigidbody2D>().velocity.y);
    }

    
}
